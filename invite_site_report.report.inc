<?php

/**
 * @file
 * The logic and theming functions for the invite site report.
 */

function invite_site_report_report_overview($page = 'accepted') {

  $vars = array();
	
	// Generate counts for number of accepted, pending, expired, and total invitations.
  $invites = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {invite}"));
  $vars['total_invites'] = $invites['count'];
  $invites = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {invite} i LEFT JOIN {users} u ON u.uid = i.mid AND u.uid <> 0 WHERE i.timestamp > 0"));
  $vars['accepted_invites'] = $invites['count'];
  $invites = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {invite} WHERE timestamp = 0 AND expiry >= %d", time()));
  $vars['pending_invites'] = $invites['count'];
  $invites = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {invite} WHERE timestamp = 0 AND expiry < %d", time()));
  $vars['expired_invites'] = $invites['count'];
	
  $time = time();
  $profile_access = user_access('access user profiles');
  $allow_delete = user_access('withdraw accepted invitations');

  switch ($page) {
    case 'accepted':
    default:
      $sql = "SELECT i.*, u.uid FROM {invite} i LEFT JOIN {users} u ON u.uid = i.mid AND u.uid <> 0 WHERE i.timestamp > 0 ORDER BY u.uid DESC, i.expiry DESC";
      break;
    case 'pending':
      $sql = "SELECT * FROM {invite} WHERE timestamp = 0 AND expiry >= %d ORDER BY expiry DESC";
      break;
    case 'expired':
      $sql = "SELECT * FROM {invite} WHERE timestamp = 0 AND expiry < %d ORDER BY expiry DESC";
      break;
  }

  $result = pager_query($sql, 50, 0, NULL, $time);

  while ($invite = db_fetch_object($result)) {
    $row = array();
    switch ($page) {
      case 'accepted':
      default:
        $account_exists = !is_null($invite->uid);

        if ($profile_access) {
          $row[] = $account_exists ? l($invite->email, 'user/'. $invite->mid, array('title' => t('View user profile.'))) : check_plain($invite->email);
        }
        else {
          $row[] = check_plain($invite->email);
        }
        $row[] = $account_exists ? t('Accepted') : t('Deleted');
        $row[] = $allow_delete ? l(t('Withdraw'), "invite/withdraw/{$page}/{$invite->reg_code}") : '';
        break;

      case 'pending':
      case 'expired':
        $expired = ($invite->expiry < $time);

        $row[] = check_plain($invite->email);
        $row[] = $expired ? t('Expired') : t('Pending');
        $row[] = l(t('Withdraw'), "invite/withdraw/{$page}/{$invite->reg_code}");
        break;
    }
    $vars['rows'][] = $row;
  }

  return theme('invite_site_report_report_overview', $vars);
}

/**
 * Display the invite site report.
 */
function theme_invite_site_report_report_overview($vars) {

  $items = $vars['rows'];

  $output = '<div id="invite-site-report-totals" class="item-list"><h3>'. t('Totals') .'</h3><ul>';
  $output .= '<li>'. t('Total Invitations: %total', array('%total' => $vars['total_invites'])) .'</li>';
  $output .= '<li>'. t('Total Accepted Invitations: %total', array('%total' => $vars['accepted_invites'])) .'</li>';
  $output .= '<li>'. t('Total Pending Invitations: %total', array('%total' => $vars['pending_invites'])) .'</li>';
  $output .= '<li>'. t('Total Expired Invitations: %total', array('%total' => $vars['expired_invites'])) .'</li>';
  $output .= '</ul></div>';

  if (count($items) > 0) {
    $headers = array(t('E-mail'), t('Status'), '');
    $output  .= theme('table', $headers, $items, array('id' => 'invites'));
    $output .= theme('pager', NULL, 50, 0);
  }
  else {
    $output .= t('No invitations available.');
  }

  return $output;
}